import React from "react";
import PropTypes from "prop-types";
import "./App.css";
import withStyles from "@material-ui/core/styles/withStyles";
import Button from "@material-ui/core/Button/Button";
import Grow from "@material-ui/core/Grow/Grow";
import { BrowserRouter as Router } from "react-router-dom";
import Route from "react-router-dom/Route";
import CardsView from "./CardsView";

const styles = {
  App: {
    backgroundColor: "#282c34",
    height: "100%",
    display: "flex",
    justifyContent: "center",
    alignItems: "center"
  },
  playBtn: {
    color: "white",
    backgroundColor: "#007bff",
    width: 100,
    "&:hover": {
      backgroundColor: "#0062cc"
    }
  }
};

const  PlayButton = ({ classes }) => {
  return (
    <Grow in timeout={2000}>
      <Button variant="contained" className={classes.playBtn} href="/game">
        Play
      </Button>
    </Grow>
  );
};

PlayButton.defaultProps = {
    classes: {}
};

PlayButton.propTypes = {
    classes: PropTypes.object // eslint-disable-line react/forbid-prop-types
};

function App(props) {
  const { classes } = props;
  return (
    <div className={classes.App}>
      <Router>
        <Route exact path="/" component={() => <PlayButton classes={classes} />} />
        <Route exact path="/game" component={CardsView} />
      </Router>
    </div>
  );
}

App.defaultProps = {
  classes: {}
};

App.propTypes = {
  classes: PropTypes.object // eslint-disable-line react/forbid-prop-types
};

export default withStyles(styles)(App);
